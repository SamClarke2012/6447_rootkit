
#include <linux/module.h>  // Module definitions
#include <linux/kernel.h>  // Kernel definitions
#include <linux/init.h>    // init / exit macros

#define DRIVER_AUTHOR 
#define DRIVER_DESC   


static int __init helloInit( void ) {
    printk( KERN_INFO "6447 rootkit installed!\n" );
    return 0;
}

static void __exit helloExit( void ) {
    printk( KERN_INFO "6447 rootkit removed!\n" );
}

module_init( helloInit );
module_exit( helloExit );

// Module license specifics to avoid kernel taint - that is, 
// non GPL licensed drivers are flagged by the kernel.
MODULE_LICENSE( "GPL" );

MODULE_AUTHOR( "Sam Clarke <s.clarke@student.unsw.edu.au>" );

MODULE_DESCRIPTION( "A basic kernel module, or, a foothold into building a rootkit" );




/*

root@debian:/home/user/share/kernel_module# insmod hello.ko 

root@debian:/home/user/share/kernel_module# dmesg
[13797.638822] 6447 rootkit installed!

root@debian:/home/user/share/kernel_module# rmmod hello 

root@debian:/home/user/share/kernel_module# dmesg
[13797.638822] 6447 rootkit installed!
[13808.761393] 6447 rootkit removed!

*/
