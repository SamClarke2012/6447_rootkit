/* 3.2.0 */
#include <linux/string.h>
#include <linux/crypto.h>
#include <linux/module.h>
#include <linux/random.h>

// Block size is always 16, 
// regardless of key size
#define BLK_SZ 16
#define IV_SZ BLK_SZ


// Calculate the number of AES blocks for plaintext
static int nblocks( int len ) {
	int blocks = 0;
	if( (len % BLK_SZ) == 0 ) return len / BLK_SZ;
	// Nearest greater multiple of BLK_SZ
	while( blocks*BLK_SZ < len ) blocks++;
	// Return number of blocks
	return blocks;
}


static void gen_iv( char *buf, char *pt, int len ) {
	// Create [IV + plaintext]
	for(int i=0; i<IV_SZ; i++) {
		get_random_bytes( &buf[i], sizeof(char));
	}
	for(int i=IV_SZ; i<len; i++) {
		buf[i] = pt[i - IV_SZ];
	}
	return;
}

// Create an AES cipher struct
static struct crypto_cipher *init_encryption( char *key ) {

	// Create AES BLK cipher (asynchronous)
	struct crypto_cipher *tfm;
	tfm = crypto_alloc_cipher( "aes", 
		           CRYPTO_ALG_TYPE_BLKCIPHER, 
		           CRYPTO_ALG_ASYNC);
	if( !tfm ) {
		printk("Unable to allocate cipher struct");
		return NULL;
	}
	// Set key
	crypto_cipher_setkey( tfm, key, sizeof(key) );
	// Return ptr
	return tfm;
}


// Encrypt some plaintext - allocates memory for ciphertext, returns ptr
static char *encrypt( struct crypto_cipher *tfm, char *plaintext, int len ) {

	// Calculate blocks of ciphertext
	int blocks = nblocks( len );
	// Allocate memory
	char *c = kmalloc( 16 * blocks * sizeof(char), GFP_KERNEL );
	if( !c ) return NULL;
	// Encrypt each block
	for(int i=0; i<blocks; i++) {
		crypto_cipher_encrypt_one( tfm, &c[16 * i], &plaintext[16 * i] );
	}
	// Return ptr to ciphertext
	return c;
}


// Decrypt ciphertext in-place - ciphertext is replaced by plaintext
static void decrypt( struct crypto_cipher *tfm, char *ciphertext, int len ) {

	// Calculate number of blocks
	int blocks = nblocks( len );
	// Decrypt each block
	for(int i=0; i<blocks; i++) {
		crypto_cipher_decrypt_one( tfm, &ciphertext[16 * i], &ciphertext[16 * i] );
	}
	return;
}


int _init(void) {

	// Key size determines cipher strength
	// 32 bytes for AES-256. 
	char key[] 	     = "12345678901234567890123456789012";
	char plaintext[] = "\xde\xad\xbe\xef"
					   "ThIs_Is_5um_data"
					   "\xde\xad\xbe\xef"
					   "\xde\xad\xbe\xef";
	// Plaintext size + iv
	int len = sizeof(plaintext) + IV_SZ;
	printk("Len: %d", len);
	// Init cipher 
	struct crypto_cipher *tfm = init_encryption( key );
	// Encrypt plaintext + iv
	char pt[ len ];
	gen_iv( pt, plaintext, len );

	char *ct = encrypt( tfm, pt, len );
	printk("%45pEh", ct);

	// Decrypt ciphertext
	decrypt( tfm, ct, len );
	printk("%45pEh", ct);

	// Free cipher
	crypto_free_cipher( tfm );
	printk(" ");
	
	return 0;
}

void _ext(void) {
	return;
}

module_init(_init);
module_exit(_ext);

MODULE_LICENSE("GPL");




