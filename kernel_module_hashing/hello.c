/* 3.2.0 */
#include <linux/string.h>
#include <linux/module.h>
#include <crypto/internal/hash.h>

#define SHA256_SZ 32


// Hash of 'test'
// perl -e 'printf "test"' | sha256sum
char *test = "\x9f\x86\xd0\x81\x88\x4c\x7d\x65"
             "\x9a\x2f\xea\xa0\xc5\x5a\xd0\x15"
             "\xa3\xbf\x4f\x1b\x2b\x0b\x82\x2c"
             "\xd1\x5d\x6c\x15\xb0\xf0\x0a\x08";

// String to hash
char *image = "test";


// Print hash
static void dump_hash(char *str, char *hash) {
	printk("[sha256] '%s' : %32phC\n", str, hash);
}

static int check_hashes( char *h1, char *h2 ) {
	int match = 1;
	for(int i=0; i<SHA256_SZ; i++) {
		if( h1[i] != h2[i] ) match = 0;
	}
	return match;
}


static int _init( void ) {

	char hash_output[ SHA256_SZ ];
	struct crypto_shash *hash_alg;
	struct shash_desc  *hash_desc;
	
	// Allocate the hash structure for sha256
	hash_alg = crypto_alloc_shash("sha256", 0, 0);
	if( IS_ERR(hash_alg) ) return -1;
	
	// Allocate space hash desc + hash state 
	hash_desc = kmalloc( sizeof(struct shash_desc) + 
		                 crypto_shash_descsize(hash_alg), GFP_KERNEL );
	if( !hash_desc ) return -1;

	// Point hash desc to alg structure
	hash_desc->tfm = hash_alg;
	// No flags
	hash_desc->flags = 0;

	// Initialise hash
	if( crypto_shash_init( hash_desc ) )   return -1;
	// Hash image with the algorithm
	if( crypto_shash_update( hash_desc, image, strlen(image)) ) return -1;
	// Finalise and move into hash_output
	if( crypto_shash_final( hash_desc, hash_output ) )  return -1;

	// Free hash structs
	kfree( hash_desc );
	crypto_free_shash( hash_alg );

	// Check against test hash
	if( check_hashes( test, hash_output ) ) {
		printk("Hashes match - test passed");
	} else {
		printk("Hashes don't match = test failed");
	}
	dump_hash( image, hash_output );

	return 0;
}



void _ext(void)
{
}

module_init(_init);
module_exit(_ext);

MODULE_LICENSE("GPL");


