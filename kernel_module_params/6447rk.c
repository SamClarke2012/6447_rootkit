
#include <linux/module.h>       // Module definitions
#include <linux/kernel.h>       // Kernel definitions
#include <linux/init.h>         // init / exit macros
#include <linux/moduleparam.h>  // module_param


// It's possible to strip .ko modules - but not fully
// you can only remove debug symbols, but this does
// increase the opsec somewhat.
// strip --strip-debug module.ko


// Define to remove debug for build
// - we don't want strings to reveal anything about us
// - Perhaps some decoy strings should be defined 
// - e.g: 
//        char *dc = "Intel microcode v%.2f installed\n"
// - then spoof Intel in the module author etc
#define RTK_DEV



// Param permissions - don't register with sysfs
// - no /sys/module/parameters/param file
// - Even if read only, it's possible to alter
// - the params using vi
// - see: https://unix.stackexchange.com/questions/214893/why-am-i-able-to-write-a-module-parameter-with-read-only-permissions
#define NO_SYSFS 0 // Parameter permissions


// It's possible for global param names to conflict with
// those of other modules  - name carefully
// passing string parameters to the module requires quotes
// at the value passed to the kernel - not the shell so
// insmod hello.ko rtk_str='"Hello from the 6447 rootkit!!"'
// Note the '""' 
int rtk_int = 1337;
module_param( rtk_int, int, NO_SYSFS );

char *rtk_str = "[6447 rootkit]";
module_param( rtk_str, charp, NO_SYSFS );

static void someotherfunc( void );

// Load / Unload functions
// - called when insmod/rmmod invoked
// - check with lsmod | grep hello
static int __init helloInit( void ) {
#ifdef RTK_DEV
    printk( KERN_ALERT "[6447 rootkit] Implant Installed!\n" );
    printk( KERN_INFO "[6447 rootkit] rtk_int: %d\n", rtk_int );
    printk( KERN_INFO "[6447 rootkit] rtk_str: %s\n", rtk_str );
    someotherfunc();
#endif
    return 0;
}

static void __exit helloExit( void ) {
#ifdef RTK_DEV
    	printk( KERN_ALERT "[6447 rootkit] Implant Removed!\n" );
#endif
}


static void someotherfunc( void ) {
    printk( KERN_INFO "[6447 rootkit] External Function Called\n" );
    for( int i=0; i < 11; i++ ) {
        printk( KERN_INFO "[6447 rootkit] External Cycle %d\n", i );
    }
}

// Link functions on module load/unload
module_init( helloInit );
module_exit( helloExit );




// Module license specifics to avoid kernel taint - that is, 
// non GPL licensed drivers are flagged by the kernel.
// Put down here to get it out of the way - no other reason
MODULE_LICENSE( "GPL" );

MODULE_AUTHOR( "Sam Clarke <s.clarke@student.unsw.edu.au>" );

MODULE_DESCRIPTION( "A basic kernel module, or, a foothold into building a rootkit" );














/*
[38036.701305] [6447 rootkit] Implant Installed!
[38036.701309] [6447 rootkit] rtk_int: 1337
[38036.701309] [6447 rootkit] rtk_str: [6447 rootkit]
[38036.701310] [6447 rootkit] External Function Called
[38036.701310] [6447 rootkit] External Cycle 0
[38036.701310] [6447 rootkit] External Cycle 1
[38036.701311] [6447 rootkit] External Cycle 2
[38036.701311] [6447 rootkit] External Cycle 3
[38036.701312] [6447 rootkit] External Cycle 4
[38036.701312] [6447 rootkit] External Cycle 5
[38036.701312] [6447 rootkit] External Cycle 6
[38036.701313] [6447 rootkit] External Cycle 7
[38036.701313] [6447 rootkit] External Cycle 8
[38036.701313] [6447 rootkit] External Cycle 9
[38036.701314] [6447 rootkit] External Cycle 10
[38041.699543] [6447 rootkit] Implant Removed!
*/
